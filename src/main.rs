use dizzav_backend::{evaluation, parsing::parse, typing::type_string};
use std::{collections::HashMap, thread, time::Duration};

fn main() {
	let input_string = std::env::args().nth(1).unwrap();
	let parsed_input = parse(&input_string).unwrap();
	let mut command_handlers = HashMap::new();
	command_handlers.insert("type".to_string(), &|args: &[Box<_>]| {
		if args.len() != 1 {
			return Err(());
		}
		if let evaluation::Value::String(string) = &*args[0] {
			type_string(&string);
			Ok(evaluation::Value::Unit)
		} else {
			Err(())
		}
	});
	thread::sleep(Duration::from_secs(1));
	evaluation::eval(&parsed_input, &command_handlers).unwrap();
}

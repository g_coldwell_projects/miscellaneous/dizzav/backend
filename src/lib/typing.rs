use enigo::Enigo;
use enigo::KeyboardControllable;

pub fn type_string(string: &str) {
	let mut enigo = Enigo::new();
	enigo.key_sequence(string);
}

use sexp::Atom;

#[derive(Debug, Clone)]
pub enum Value {
	Unit,
	Integer(i64),
	Float(f64),
	String(String),
}

impl From<&Atom> for Value {
	fn from(atom: &Atom) -> Value {
		match atom {
			Atom::I(value) => Value::Integer(*value),
			Atom::F(value) => Value::Float(*value),
			Atom::S(value) => Value::String(value.to_string()),
		}
	}
}

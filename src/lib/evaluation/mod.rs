pub mod value;

use sexp::{Atom, Sexp};
use std::collections::HashMap;
pub use value::Value;

pub type HandlerResult = Result<Value, ()>;

pub fn eval<S: std::hash::BuildHasher>(
	sexp: &Sexp,
	commands: &HashMap<String, impl Fn(&[Box<Value>]) -> HandlerResult, S>,
) -> Result<Value, ()> {
	match sexp {
		Sexp::Atom(atom) => {
			//			println!("{:?}", atom);
			Ok(atom.into())
		}
		Sexp::List(values) => {
			println!("{:?}", values);
			if values.is_empty() {
				Ok(Value::Unit)
			} else {
				let evaluated_values = values[1..]
					.iter()
					.map(|value| Box::new(eval(value, commands).unwrap()))
					.collect::<Vec<_>>();
				commands
					.get(match &values[0] {
						Sexp::Atom(value) => match value {
							Atom::F(_) => return Err(()),
							Atom::I(_) => return Err(()),
							Atom::S(function_name) => function_name,
						},
						Sexp::List(_) => return Err(()),
					})
					.unwrap()(&evaluated_values)
			}
		}
	}
}
